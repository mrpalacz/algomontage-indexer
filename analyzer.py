'''
Analyzer
Iterates through a video file and gathers information about it

'''

import os
import imutils
import re
import math
import cv2 as cv
import numpy as np
import logging
from collections import deque
from matplotlib import pyplot as plt
from multiprocessing import Pool, current_process
from tqdm import tqdm
from scenedetect.video_manager import VideoManager
from datetime import datetime

from database import VideoDatabase
from analyzers.colors import ColorAnalyzer
from analyzers.movements import MovementAnalyzer
from analyzers.scenes import SceneAnalyzer

num_cpus = cv.getNumberOfCPUs()
max_duration = 3.0
logger = logging.getLogger('main')

class Analyzer:
  def __init__(self, path, preview=False):

    # Measure how long we take
    self.start_time = datetime.now()

    # Instantiate instance vars
    self.preview, self.path = preview, path

    # Start working with the file
    self.name = os.path.basename(path)

    # Pool is used for multiprocessing
    self.pool = Pool(num_cpus)
    self.pending = deque()

    # Setup VideoManager
    try:
      self.video = VideoManager([path])
    except:
      logger.error(f"Error opening {self.name}")
      return
    self.video.set_downscale_factor()
    base_timecode = self.video.get_base_timecode()

    # Initialize frame counters and image buffers
    curr_frame = 0
    total_frames = math.trunc(self.video.get(cv.CAP_PROP_FRAME_COUNT))
    start_frame = None
    scene_frames = deque()
    logger.info(f"Total frames: {total_frames}")

    # Query database for last processed frame
    self.db = VideoDatabase(path, base_timecode.get_framerate())
    last_processed = self.db.get_last_processed_frame()

    # Test if all frames have been processed, add 10 frames generously
    if last_processed is not None and (last_processed+10) >= total_frames:
      logger.info("Already completely processed!")
      return

    # Setup progressbar
    pbar = tqdm(total=total_frames, unit='frames', position=0)

    # Start reading the video
    self.video.start()

    # Fast forward to last processed frame
    if last_processed is not None:
      pbar.set_description(f"Seeking to frame {last_processed}")
      if not self.video.seek(base_timecode+last_processed):
        logger.error(f"Seeking failed for {self.name} / {last_processed}")
        return
      curr_frame = last_processed
      pbar.update(last_processed)
      pbar.set_description()

    # Setup analyzers
    scene_analyzer = SceneAnalyzer()

    while True:
      # Check for pending processes first and process them
      self.process_pending()

      # If there are too many processes wait
      if len(self.pending) >= num_cpus: continue

      # Read current frame and exit if not successful
      success, frame_img = self.video.read()
      if not success:
        # If it is within the video log error
        if curr_frame < total_frames:
          logger.error(f"Reading failed for {self.name} / {curr_frame}")
        break

      # Process frame with scene analyzer
      scene_analyzer.process_frame(curr_frame, frame_img)
      should_calc = scene_analyzer.is_new_scene

      # Calculate if it is the last frame of the video
      should_calc = should_calc or (curr_frame+1) >= total_frames

      # If the duration of a scene is too long divide it
      if start_frame is not None:
        duration = base_timecode+(curr_frame-start_frame)
        too_long = duration.get_seconds() > max_duration
        should_calc = should_calc or too_long

      # Should we calculate the heavy stuff?
      if should_calc:

        # Create a tuple with the timecodes of the scene
        clip = (base_timecode+start_frame), (base_timecode+curr_frame)
        logger.debug("Calculating for clip "
                     f"{clip[0].get_timecode()} / {clip[0].get_frames()} – "
                     f"{clip[1].get_timecode()} / {clip[1].get_frames()}")

        if (self.db.last_frame['colors'] or 0) < curr_frame:
          # Create a new Process which will analyse the frame for color
          color_analyzer = ColorAnalyzer()
          task = self.pool.apply_async(
              color_analyzer.calculation, (scene_frames[0], clip))
          self.pending.append(task)

        if (self.db.last_frame['movements'] or 0) < curr_frame:
          # Create a new Process which will analyse the frame for color
          movement_analyzer = MovementAnalyzer()
          task = self.pool.apply_async(
              movement_analyzer.calculation, (scene_frames.copy(), clip))
          self.pending.append(task)

      # Save image and framenumber of first frame of scene (and first frame)
      if should_calc or start_frame is None:
        start_frame = curr_frame
        scene_frames.clear()

      # Show image if we preview
      if self.preview:
        key = cv.waitKey(1) & 0xFF
        if key == ord("q"): break
        cv.imshow("Frame", frame_img)

      # Add to scene deque
      scene_frames.append(frame_img)

      # Increment counter and progressbar
      curr_frame += 1
      pbar.update()

  def process_pending(self):
    # Check for pending processes first and process them
    while len(self.pending) > 0 and self.pending[0].ready():
      analyzer = self.pending.popleft().get()
      self.db.insert_rows(*analyzer.insert)
      if self.preview:
        analyzer.preview()

  def __del__(self):
    # Called before object is destroyed

    # Release Video Manager
    self.video.release()

    # Close Pool
    self.pool.close()
    self.pool.join()

    # Insert pending calculations
    self.process_pending()

    delta = datetime.now()-self.start_time
    logger.info(f"Finished: {self.name}, inserted {self.db.insert_count} rows, took {delta}")

