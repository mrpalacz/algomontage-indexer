'''
Algomontage Indexer
This package allows indexing of video files for various aspects

'''

import os, logging
from glob import glob
from argparse import ArgumentParser
from tqdm import tqdm

from analyzer import Analyzer

logger = logging.getLogger('main')

# Create a parser for the command line
parser = ArgumentParser(description='Algomontage Indexer | Indexing video files the hard way 🔨🎬🔎')
parser.add_argument("-i", "--input", type=str, help="Path to input video file")
parser.add_argument("-f", "--folder", type=str, help="Path to folder with videos")
parser.add_argument("-r", "--resume", type=int, default=1, dest="resume_idx",
                    help="Start with given index in folder")
parser.add_argument("-p", "--preview", type=bool, help="Show preview of frame image")
parser.add_argument("-v", "--verbose", dest="verbose_count",
                    action="count", default=0,
                    help="increases log verbosity for each occurence")
args = parser.parse_args()

# Setup logging
logging.basicConfig(format="%(asctime)s [%(filename)s:%(lineno)d] %(message)s",
                    datefmt='%Y-%m-%d/%H:%M:%S')
# Sets log level to WARN going more verbose for each new -v.
logger.setLevel(max(3 - args.verbose_count, 0) * 10)

# Process a single file
if args.input is not None:
  print(f"——— Processing: {os.path.basename(args.input)}")
  Analyzer(args.input, preview=args.preview)

# Process a folder
elif args.folder is not None:
  # Check if folder exists
  if not os.path.exists(args.folder):
    raise Exception(f"Folder not found: {args.folder}")

  # This pattern collects the video file extensions
  pattern = "**/*.[Mm][PpOo4][4Vv]"  # .mov, .mp4, .m4v
  files = sorted(glob(os.path.join(args.folder, pattern), recursive=True))

  # Iterate over all the files
  for idx, file in enumerate(files, 1): # start with 1
    if args.resume_idx > idx: continue
    print(f"——— Processing {idx}/{len(files)}: {os.path.basename(file)}")
    Analyzer(file, preview=args.preview)
