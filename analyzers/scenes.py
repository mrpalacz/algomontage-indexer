import numpy
import cv2 as cv

class SceneAnalyzer:
    '''
    Detects fast cuts using changes in colour and intensity between frames.
    Since the difference between frames is used, unlike the ThresholdDetector,
    only fast cuts are detected with this method.  To detect slow fades between
    content scenes still using HSV information, use the DissolveDetector.
    '''

    def __init__(self, threshold=30.0, min_scene_len=15):
        self.threshold = threshold
        # minimum length of any given scene, in frames (int) or FrameTimecode
        self.min_scene_len = min_scene_len
        self.last_frame = None
        self.last_scene_cut = None
        self.previous_scene_cut = None
        self.last_hsv = None
        self.first_compared = False

    def process_frame(self, frame_num, frame_img):
        self.is_new_scene = False
        # Initialize last scene cut point at the beginning of the frames of interest.
        if self.last_scene_cut is None:
            self.last_scene_cut = frame_num

        # We can only start detecting once we have a frame to compare with.
        if self.last_frame is not None:
            self.first_compared = True
            # Change in average of HSV (hsv), (h)ue only, (s)aturation only, (l)uminance only.
            # These are refered to in a statsfile as their respective self._metric_keys string.
            delta_hsv_avg, delta_h, delta_s, delta_v = 0.0, 0.0, 0.0, 0.0


            num_pixels = frame_img.shape[0] * frame_img.shape[1]
            curr_hsv = cv.split(cv.cvtColor(
                frame_img, cv.COLOR_BGR2HSV))
            last_hsv = self.last_hsv
            if not last_hsv:
                last_hsv = cv.split(cv.cvtColor(
                    self.last_frame, cv.COLOR_BGR2HSV))

            delta_hsv = [0, 0, 0, 0]
            for i in range(3):
                num_pixels = curr_hsv[i].shape[0] * curr_hsv[i].shape[1]
                curr_hsv[i] = curr_hsv[i].astype(numpy.int32)
                last_hsv[i] = last_hsv[i].astype(numpy.int32)
                delta_hsv[i] = numpy.sum(
                    numpy.abs(curr_hsv[i] - last_hsv[i])) / float(num_pixels)
            delta_hsv[3] = sum(delta_hsv[0:3]) / 3.0
            delta_h, delta_s, delta_v, delta_hsv_avg = delta_hsv

            self.last_hsv = curr_hsv

            # We consider any frame over the threshold a new scene, but only if
            # the minimum scene length has been reached (otherwise it is ignored).
            if delta_hsv_avg >= self.threshold and (
                    (frame_num - self.last_scene_cut) >= self.min_scene_len):
                self.is_new_scene = True
                self.previous_scene_cut = self.last_scene_cut
                self.last_scene_cut = frame_num

            if self.last_frame is not None:
                del self.last_frame


        self.last_frame = frame_img.copy()

