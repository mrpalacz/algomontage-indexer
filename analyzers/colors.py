import cv2 as cv
import numpy as np
import colorsys
from sklearn.cluster import MiniBatchKMeans
import logging

import database

logger = logging.getLogger('main')

class ColorAnalyzer:
  '''
  Color Analyzer
  Adds funtionality to index colors of the film to be analyzed
  '''
  def __init__(self):
    self.data = None
    self.scene = None
    self.cluster = MiniBatchKMeans(n_clusters=6, max_iter=5, n_init=1)

  def calculation(self, frame, scene):
    ''' Calculate the most dominant colors with KMeans '''

    # Convert the image into a RGB Vector
    rgb = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
    img_vector = rgb.reshape((rgb.shape[0] * rgb.shape[1], 3))

    # Calculate the cluster
    self.cluster.fit(img_vector)

    # Calculate histogram
    labels = np.arange(0, len(np.unique(self.cluster.labels_)) + 1)
    (hist, _) = np.histogram(self.cluster.labels_, bins=labels)
    hist = hist.astype("float")
    hist /= hist.sum()

    # Strip the commas from the color values as we are not insane
    colors = self.cluster.cluster_centers_.astype("int")

    # Sort the color values by percent
    data = []
    try:
      data = sorted([(percent, color)
              for (percent, color) in zip(hist, colors)])
      data.reverse()
    except ValueError:
      logger.debug(f"ValueError: {hist} {colors}")

    logger.debug(f"Found {len(colors)} colors "
      f"for {scene[0].get_timecode()} - {scene[1].get_timecode()}")

    self.data = data
    self.scene = scene
    self.insert = self._insert()
    return self

  def _insert(self):
    ''' Create insert statements for the database '''

    output = []

    # Iterate through every color found and insert to database
    for (percent, color) in self.data:
      # Calculate HSV Values
      r, g, b = (v / 255.0 for v in color)
      h, s, v = colorsys.rgb_to_hsv(r, g, b)

      logger.debug(f"{(percent * 100):5.1f}% "
                   f"RGB({(r*255):3.0f} {(g*255):3.0f} {(b*255):3.0f}) "
                   f"HSV({(h):0.2f} {(s):0.2f} {(v):0.2f})")

      # Insert values in database
      output.append({'start': self.scene[0].get_seconds(),
                    'end': self.scene[1].get_seconds(),
                    'start_frame': self.scene[0].get_frames(),
                    'end_frame': self.scene[1].get_frames(),
                    'h': h,
                    's': s,
                    'b': v,
                    'dominance': percent})
    return database.colors, output
