import cv2 as cv
import numpy as np
import logging

import database

logger = logging.getLogger('main')

# Number of subdivisions for optical point grid
pieces = 20
# Parameters for lucas kanade optical flow
lk_params = dict(winSize=(20, 20),
                 maxLevel=2,
                 criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))

class MovementAnalyzer:
  '''
  Color Analyzer
  Adds funtionality to index colors of the film to be analyzed
  '''

  def __init__(self):
    self.output = []

  def calculation(self, scene_frames, scene):
    ''' Calculate the movements of the image '''
    # As we pop the deque save the number of frames first
    total_frames = len(scene_frames)
    # Get the first frame and convert it to gray
    last_frame = cv.cvtColor(scene_frames.popleft(), cv.COLOR_BGR2GRAY)
    # Create a numpy grid with all the coordinates
    height, width = last_frame.shape[:2]
    start = np.mgrid[width/pieces/2:width-width/pieces/2:(pieces*1j),
                    height/pieces/2:height-height/pieces/2:(pieces*1j)].T.reshape(
                      -1, 1, 2).astype("float32")
    # Set the initial grid for optical flow estimation
    p0 = start
    # Inititialize the array for calculating the means and save strart time
    means = np.array([])
    start_time = scene[0]
    fps = scene[0].get_framerate()
    # Iterate over all the frames received
    for idx, frame_color in enumerate(scene_frames, 2):
      frame = cv.cvtColor(frame_color, cv.COLOR_BGR2GRAY)
      # Calculate optical flow
      p1, st, err = cv.calcOpticalFlowPyrLK(
          last_frame, frame, p0, None, **lk_params)
      # Add new row for current entry
      means = np.append(means, np.zeros((4, 2))).reshape((-1, 4, 2))
      # Zip old and new points together and iterate over them
      for (new, old) in zip(p1, p0):
        a, b = new.ravel()
        c, d = old.ravel()
        # Find out where the point resides
        # TODO could be done more elegantly
        if c < width/2 and d < height/2:
          pos = 0  # NE
        elif c >= width/2 and d < height/2:
          pos = 1  # NW
        elif c < width/2 and d >= height/2:
          pos = 3  # SW
        elif c >= width/2 and d >= height/2:
          pos = 2  # SE
        means[-1][pos] += ((new.ravel() - old.ravel()) * (1/pieces))

      # Save the frame for the next iteration
      last_frame = frame.copy()

      # If a second has passed or it is the last frame
      if len(means) >= int(fps) or (idx) >= total_frames:
        # Calculate the in & out time of the clip
        clip = (start_time, scene[0] + idx)
        # Calculate the mean
        mean = means.mean(0)
        # Convert the x,y vectors to magnitude and angle
        mag, angle = cv.cartToPolar(
            mean[..., 0], mean[..., 1] * -1, angleInDegrees=True)
        # Refactor the data in a dict for db entry
        data = dict(nw_mag=mag.flatten()[0], nw_ang=angle.flatten()[0],
                    ne_mag=mag.flatten()[1], ne_ang=angle.flatten()[1],
                    se_mag=mag.flatten()[2], se_ang=angle.flatten()[2],
                    sw_mag=mag.flatten()[3], sw_ang=angle.flatten()[3],
                    start=clip[0].get_seconds(),
                    end=clip[1].get_seconds(),
                    start_frame=clip[0].get_frames(),
                    end_frame=clip[1].get_frames(),
                    )

        logger.debug("Movement: "
                     f"{data['start_frame']} - {data['start_frame']}: "
                    f"NW[{data['nw_mag']:3.2f}/{data['nw_ang']:3.2f}] "
                    f"NE[{data['ne_mag']:3.2f}/{data['ne_ang']:3.2f}] "
                    f"SE[{data['se_mag']:3.2f}/{data['se_ang']:3.2f}] "
                    f"SW[{data['sw_mag']:3.2f}/{data['sw_ang']:3.2f}] "
                    )
        self.output.append(data)
        # Set start time to end time
        start_time = clip[1]
        # Reset the point mask to start
        p0 = start
        # Create a new numpy array
        means = np.array([])

    self.insert = database.movements, self.output
    return self
