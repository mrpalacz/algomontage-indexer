'''
Database
Hander to interact with the SQL database
We are using sqlalchemy to be able to use different engines

'''

import os, colorsys, logging
import sqlalchemy as db
from sqlalchemy import MetaData, Table, Column, Integer, String, ForeignKey, Float

logger = logging.getLogger('main')

engine = db.create_engine('sqlite:///../algomontage.sqlite')
connection = engine.connect()
metadata = MetaData()

files = Table('files', metadata,
              Column('id', Integer(), primary_key=True, autoincrement=True),
              Column('name', String(), nullable=False),
              Column('size', String(), nullable=False),
              Column('fps', Float(), nullable=False),
              )

dirs = Table('dirs', metadata,
              Column('dir', String(), nullable=False))

colors = Table('colors', metadata,
              Column('file_id', Integer(), ForeignKey("files.id"), nullable=False),
              Column('start', Float(), nullable=False),
              Column('end', Float(), nullable=False),
              Column('start_frame', Integer(), nullable=False),
              Column('end_frame', Integer(), nullable=False),
              Column('h', Float(), nullable=False),
              Column('s', Float(), nullable=False),
              Column('b', Float(), nullable=False),
              Column('dominance', Float(), nullable=False),
              )

movements = Table('movements', metadata,
               Column('file_id', Integer(), ForeignKey("files.id"), nullable=False),
               Column('start', Float(), nullable=False),
               Column('end', Float(), nullable=False),
               Column('start_frame', Integer(), nullable=False),
               Column('end_frame', Integer(), nullable=False),
               Column('nw_mag', Float(), nullable=False),
               Column('nw_ang', Float(), nullable=False),
               Column('ne_mag', Float(), nullable=False),
               Column('ne_ang', Float(), nullable=False),
               Column('se_mag', Float(), nullable=False),
               Column('se_ang', Float(), nullable=False),
               Column('sw_mag', Float(), nullable=False),
               Column('sw_ang', Float(), nullable=False),
               )

metadata.create_all(engine) # Creates the table


class VideoDatabase:
  ''' database handler for a specific video file '''
  def __init__(self, path, fps):
    if not os.path.exists(path):
      raise Exception('File not found: {}'.format(path))
    self.name = os.path.basename(path)
    self.dir = os.path.dirname(os.path.abspath(path))
    self.size = os.path.getsize(path)
    self.fps = fps
    self.insert_count = 0

    self.setup_name()
    self.setup_dir()

    self.last_frame = dict(colors=self.get_last_frame(colors),
                           movements=self.get_last_frame(movements))

  def setup_name(self):
    # Search if the file already exists in the db
    query = db.select([files]).where(db.and_(files.columns.size ==
                                             self.size, files.columns.name == self.name))
    ResultProxy = connection.execute(query)
    ResultSet = ResultProxy.fetchone()

    if ResultSet is not None: # it exists, so populate file_id
      logger.debug(f"Already in database: {self.name}")
      self.file_id = ResultSet['id']

    else:  # it does not exist, so create it
      logger.debug(f"Inserting file: {self.name}")
      query = db.insert(files).values(name=self.name, size=self.size, fps=self.fps)
      ResultProxy = connection.execute(query)
      self.insert_count += 1
      self.file_id = ResultProxy.lastrowid


  def setup_dir(self):
    # Search if the directory already exists in the db
    query = db.select([dirs]).where(dirs.columns.dir == self.dir)
    ResultProxy = connection.execute(query)
    ResultSet = ResultProxy.fetchone()

    if ResultSet is None: # if it does not exist we create it
      logger.debug(f"Inserting dir: {self.dir}")
      query = db.insert(dirs).values(dir=self.dir)
      self.insert_count += 1
      ResultProxy = connection.execute(query)

  def get_last_processed_frame(self):
    return self.get_last_frame(colors) and self.get_last_frame(movements)
    # TODO: Make prettier!

  def get_last_frame(self, table):
    # Querys the last frame num of the specified table
    query = db.select([db.func.max(table.columns.end_frame).label('last_frame')]).where(table.columns.file_id == self.file_id)
    ResultProxy = connection.execute(query)
    ResultSet = ResultProxy.fetchone()
    last_frame = ResultSet['last_frame']
    logger.debug(f"Last processed frame of {table}: {last_frame}")
    # Returns None if there was nothing found, else the frame num
    return last_frame

  def insert_rows(self, table, data):
    ''' Insert multiple rows into a table '''
    for row in data:
      self.insert_data(table, **row)

  def insert_data(self, table, **kwargs):
    ''' Insert a single row into a table '''

    # file_id is added to kwargs
    kwargs['file_id'] = self.file_id

    # Format floats in the dict for better logging
    kwargs_formatted = {k: round(v, 2) if isinstance(
        v, float) else v for k, v in kwargs.items()}
    logger.debug(f"Inserting in {table} for {self.name}: {kwargs_formatted}")

    # kwargs are passed directly to the insert statement
    ResultProxy = connection.execute(table.insert(), [kwargs])
    self.insert_count+=1

